package com.farkhutdinov.controller

import com.farkhutdinov.service.MathService
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.annotation.MockBean
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import spock.lang.Specification
import spock.lang.Unroll

import javax.inject.Inject

@MicronautTest
class MathCollaboratorSpec extends Specification {

    @Inject
    MathService mathService

    @Inject
    @Client('/')
    RxHttpClient client


    @Unroll
    void "should compute #num to #square"() {
        when:
        Integer result = client.toBlocking().retrieve(HttpRequest.GET('/math/compute/10'), Integer)
        then:
        1 * mathService.compute(10) >> getSum(num)
        result == square

        where:
        num | square
        2   | 3
        3   | 4
    }

    @MockBean(MathService)
    MathService mathService() {
        Mock(MathService)
    }

    Integer getSum(num){
        return num + 1;
    }
}
