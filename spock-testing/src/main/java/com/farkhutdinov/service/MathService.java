package com.farkhutdinov.service;

public interface MathService {
    Integer compute(Integer num);
}
