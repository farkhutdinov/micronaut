create table if not exists Hero
(
    id   int          not null primary key,
    name varchar(255) not null,
    constraint id unique (id),
    constraint name unique (name)
);
insert into Hero (id, name)
values (1, 'Spider-man'),
       (2, 'Dr.Strange'),
       (3, 'Halk');