package com.farkhutdinov.service;

import com.farkhutdinov.model.Hero;

import java.util.List;
import java.util.Optional;

public interface HeroService {
    Optional<Hero> findById(Long id);

    Hero save(Hero hero);

    List<Hero> findAll();

    void deleteById(Long id);
}
