package com.farkhutdinov.endpoints;

import com.farkhutdinov.model.Hero;
import com.farkhutdinov.service.HeroService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.validation.Valid;
import java.util.List;

@ExecuteOn(TaskExecutors.IO)
@Controller("/heroes")
public class HeroController {

    private final HeroService service;

    public HeroController(HeroService service) {
        this.service = service;
    }

    @Operation(summary = "Show a Hero by id",
            description = "Return Hero"
    )
    @ApiResponse(responseCode = "400", description = "Invalid Name Supplied")
    @ApiResponse(responseCode = "404", description = "Hero not found")
    @Tag(name = "show")
    @Get("/{id}")
    public Hero show(Long id) {
        return service.findById(id).orElse(null);
    }

    @Get
    public List<Hero> findAll() {
        return service.findAll();
    }

    @Post
    public HttpResponse<Hero> save(@Body @Valid Hero hero) {
        return HttpResponse.created(service.save(hero));
    }

    @Delete("/{id}")
    public HttpResponse delete(Long id) {
        service.deleteById(id);
        return HttpResponse.noContent();
    }
}